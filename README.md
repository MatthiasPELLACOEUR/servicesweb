## Projet Services Web : 

### Objectifs : 
    - Projet noté (2eme note Contrôle continu) individuellement
    - Objectif : réaliser une application client-serveur de recherche client
    - Plateforme au choix : PC, Ios, Android
    - Technologie de votre choix : PHP, C#, Java, Unity…
    - Technologie serveur peut être différente de la technologie cliente
    - Obligation de déléguer la recherche sur le serveur
    - Architecture SOAP ou REST

### Fonctionnalités : 
    - Recherche dans une base de données client, la recherche doit être faite coté 
        webservice et affichage dans une liste coté front
    - Champs : Ref contact (ID), nom, prénom, adresse , ville, code postal.

### Livrables : 
    - Archive contenant le projet client-serveur
    - Document de description technique détaillant l’architecture , le framework et les APIS utilisées.
