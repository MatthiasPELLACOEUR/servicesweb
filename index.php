<?php
session_start();
$array = [];
if(isset($_SESSION["array"])){
    $array = json_decode(base64_decode(urldecode($_SESSION["array"])), true);
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>TP Web Services</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    </head>

    <body>
        <div class="container">
            <h3><a href='./index.php' style="color:black"> Client search</a></h3>
                <form method="GET" action="./api.php" id='createForm' margin-top="15px"> 
                    <div class="form-check form-check-inline">
                            <input type="radio" name="searchOption" class="form-check-input" value="guid">
                            <label for="guid" class="form-check-label">GUID</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input type="radio" name="searchOption" class="form-check-input" value="first" checked> 
                        <label for="first" class="form-check-label">Firstname</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input type="radio" name="searchOption" class="form-check-input" value="last"> 
                        <label for="last" class="form-check-label">Lastname</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input type="radio" name="searchOption" class="form-check-input" value="street"> 
                        <label for="street" class="form-check-label">Street</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input type="radio" name="searchOption" class="form-check-input" value="city"> 
                        <label for="city" class="form-check-label">City</label>
                    </div>

                    <div class="form-check form-check-inline">
                        <input type="radio" name="searchOption" class="form-check-input" value="zip"> 
                        <label for="zip" class="form-check-label">Zip</label>
                    </div>

                    <br>
                    <div class="row g-3">
                        <div class="col-auto">
                            <input type="text" name="search" class="form-control" placeholder="Your Search">
                        </div>
                        <div class="col-auto">
                            <input type="submit" name="submit" class="btn btn-outline-primary btn-sm mb-3" value="Search">
                        </div>
                    </div>
                </form>
        </div>
        <div class="container">
            <table class="table table-bordered table-sm">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Firstname</th>
                        <th scope="col">Lastname</th>
                        <th scope="col">Street</th>
                        <th scope="col">City</th>
                        <th scope="col">Zip</th>
                    </tr>
                </thead>
                
            <?php 
                $i = 1;
                if($array != []) {
                foreach ($array as $result) : ?>
                <tbody>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $result['first']; ?></td>
                        <td><?php echo $result['last']; ?></td>
                        <td><?php echo $result['street']; ?></td>
                        <td><?php echo $result['city']; ?></td>
                        <td><?php echo $result['zip']; ?></td>
                    </tr>
                </tbody>
                
            <?php
            $i += 1; 
            endforeach;} 
            
            else{
                echo '<p style="color:red;">You have to enter a value to start the search</p>';
            }
        
            ?>
            </table>
            
        </div>
        

        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    </body>
</html>
    