<?php 
session_start();

require './connection.php';

$search = isset($_GET['search']) ? $_GET['search'] : '';
$searchOption = isset($_GET['searchOption'] ) ? $_GET['searchOption']  : '';

if($search != '' && $searchOption !='')
{

    switch ($searchOption) {
        case 'guid':
            $req = $bdd->prepare(
                "SELECT first, last, street, city, zip FROM clients
                WHERE guid like :search",
                array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
            );
            break;

        case 'first':
            $req = $bdd->prepare(
                "SELECT first, last, street, city, zip FROM clients
                WHERE first like :search",
                array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
            );
            break;

        case 'last':
            $req = $bdd->prepare(
                "SELECT first, last, street, city, zip FROM clients
                WHERE last like :search",
                array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
            );
            break;

        case 'street':
            $req = $bdd->prepare(
                "SELECT first, last, street, city, zip FROM clients
                WHERE street like :search",
                array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
            );
            break;

        case 'city':
            $req = $bdd->prepare(
                "SELECT first, last, street, city, zip FROM clients
                WHERE city like :search",
                array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
            );
            break;   

        case 'city':
            $req = $bdd->prepare(
                "SELECT first, last, street, city, zip FROM clients
                WHERE city like :search",
                array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
            );
            break; 
        case 'zip':
            $req = $bdd->prepare(
                "SELECT first, last, street, city, zip FROM clients
                WHERE zip like :search",
                array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY)
            );
            break; 
    }
    $search = '%'.$search.'%';
    $bSuccess = $req->execute(array('search' => $search));

    if($bSuccess)
    {
        $results = $req->fetchAll(PDO::FETCH_ASSOC);
        $_SESSION['array'] = urlencode(base64_encode(json_encode($results)));
        header('Location: ./index.php');
    }
}


?>

